var middleware = {};
var campSiteModel = require("../models/campsite");
var Comment = require("../models/comment");


middleware.checkCampsiteOwnership = function(req, res, next) {
    if (req.isAuthenticated()) {
        //is the user logged in
        campSiteModel.findById(req.params.id, function (err, camp) {
            if (err) {
                req.flash("error", "You need to be logged in to do that!");
            } else {
                // is the user same as the one who posted campsite
                if (camp.author.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "You don't have permission to do that!");
                    res.redirect("back");
                }
            }
        })
    } else {
        res.redirect("back");
    }
}

middleware.checkCommentOwnership = function(req, res, next) {
    if (req.isAuthenticated()) {
        //is the user logged in
        Comment.findById(req.params.comment_id, function (err, foundComment) {
            if (err) {
                req.flash("error", "Something went wrong!");
            } else {
                // is the user same as the one who posted campsite
                if (foundComment.author.id.equals(req.user._id)) {
                    next();
                } else {
                    res.redirect("back");
                }
            }
        })
    } else {
        res.redirect("/campsites/" + req.params.id);
    }
}

middleware.isLoggedIn = function(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    req.flash("error", "You need to be logged in to do that!");
    res.redirect("/login");
}

module.exports = middleware;