var mongoose = require("mongoose"),
    CampsiteModel = require("./models/campsite"),
    Comment = require("./models/comment.js")


var seeds = [{
    name: "Camp 1",
    image: "https://images.pexels.com/photos/2398220/pexels-photo-2398220.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    description: "Egestas sed sed risus pretium quam vulputate dignissim suspendisse in. Scelerisque purus semper eget duis. Urna porttitor rhoncus dolor purus non enim praesent elementum facilisis. Semper quis lectus nulla at volutpat diam. Nunc sed blandit libero volutpat sed cras ornare. Amet consectetur adipiscing elit pellentesque habitant morbi tristique. Eget nullam non nisi est sit. Nunc pulvinar sapien et ligula ullamcorper malesuada proin. Diam quam nulla porttitor massa id neque aliquam vestibulum. Risus commodo viverra maecenas accumsan lacus vel facilisis. Congue quisque egestas diam in arcu cursus euismod. blah blah"
},
{
    name: "Camp 2",
    image: "https://images.pexels.com/photos/2888108/pexels-photo-2888108.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    description: "blah Egestas sed sed risus pretium quam vulputate dignissim suspendisse in. Scelerisque purus semper eget duis. Urna porttitor rhoncus dolor purus non enim praesent elementum facilisis. Semper quis lectus nulla at volutpat diam. Nunc sed blandit libero volutpat sed cras ornare. Amet consectetur adipiscing elit pellentesque habitant morbi tristique. Eget nullam non nisi est sit. Nunc pulvinar sapien et ligula ullamcorper malesuada proin. Diam quam nulla porttitor massa id neque aliquam vestibulum. Risus commodo viverra maecenas accumsan lacus vel facilisis. Congue quisque egestas diam in arcu cursus euismod. blah"
},
{
    name: "Camp 3",
    image: "https://images.pexels.com/photos/2898221/pexels-photo-2898221.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    description: "blah Egestas sed sed risus pretium quam vulputate dignissim suspendisse in. Scelerisque purus semper eget duis. Urna porttitor rhoncus dolor purus non enim praesent elementum facilisis. Semper quis lectus nulla at volutpat diam. Nunc sed blandit libero volutpat sed cras ornare. Amet consectetur adipiscing elit pellentesque habitant morbi tristique. Eget nullam non nisi est sit. Nunc pulvinar sapien et ligula ullamcorper malesuada proin. Diam quam nulla porttitor massa id neque aliquam vestibulum. Risus commodo viverra maecenas accumsan lacus vel facilisis. Congue quisque egestas diam in arcu cursus euismod. blah"
}];



async function seedDB() {
    try {
        await Comment.remove({});
        await CampsiteModel.remove({});
        console.log("camps and comments removed");

        // for (const seed of seeds) {
        //     let campsite = await CampsiteModel.create(seed);
        //     let comment = await Comment.create(
        //         {
        //             text: "A very clever comment",
        //             author: "Old lady"
        //         }
        //     )
        //     campsite.comments.push(comment);
        //     campsite.save();
        //     console.log("one camp saved");
        //     console.log(campsite);
        // }
    } catch (err) {
        console.log(err);
    }
}
module.exports = seedDB;