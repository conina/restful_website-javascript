var express = require("express");
var router = express.Router();
campSiteModel = require("../models/campsite");
Comment = require("../models/comment");

var middleware = require("../middleware");

//INDEX - show all campsites in db
router.get("/", function (req, res) {
    campSiteModel.find({}, function (err, allCamps) {
        if (err) {
            console.log(err);
        } else {
            res.render("campsites/index", { camps: allCamps, currentUser: req.user, page: 'campgrounds' });
        }
    })
});


//CREATE - add new camsite to db
router.post("/", middleware.isLoggedIn, function (req, res) {
    var newCampName = req.body.campName;
    var newCampPhoto = req.body.campPhoto;
    var newCampPrice = req.body.campPrice;
    var author = {
        id: req.user._id,
        username: req.user.username
    }
    var newCampDescription = req.body.campDescription;
    var newCamp = {
        name: newCampName,
        price: newCampPrice,
        image: newCampPhoto,
        description: newCampDescription,
        author: author
    };
    console.log(newCamp);
    campSiteModel.create(newCamp, function (err, camp) {
        if (err) {
            console.log(err);
        } else {
            res.redirect("/campsites");
        }
    });

})

//NEW - show form to create new campsite
router.get("/new", middleware.isLoggedIn, function (req, res) {
    res.render("campsites/new");

})

//SHOW - show more detail about specific campsite
router.get("/:id", function (req, res) {
    campSiteModel.findById(req.params.id).populate("comments").exec(function (err, reqCamp) {
        if (err) {
            console.log(err);
        } else {
            console.log(reqCamp)
            res.render("campsites/show", { camp: reqCamp });
        }
    })
})

//EDIT - show a form 
router.get("/:id/edit", middleware.checkCampsiteOwnership, function (req, res) {
    campSiteModel.findById(req.params.id, function (err, camp) {
        res.render("campsites/edit", { camp: camp });
    });
})
//UPDATE - actually change the info about the campsite
router.put("/:id",middleware.checkCampsiteOwnership, function (req, res) {
    campSiteModel.findByIdAndUpdate(req.params.id, req.body.camp, function (err, updatedCamp) {
        if (err) {
            console.log(err);
        } else {
            res.redirect("/campsites/" + req.params.id);
        }
    })
})

//DELETE
router.delete("/:id", middleware.checkCampsiteOwnership, function (req, res) {
    campSiteModel.findByIdAndDelete(req.params.id, function (err) {
        if (err) {
            console.log(err);
        } else {
            res.redirect("/campsites");
        }
    })
})


module.exports = router;