var express = require("express");
var router = express.Router({ mergeParams: true });
campSiteModel = require("../models/campsite");
Comment = require("../models/comment");

var middleware = require("../middleware");

//new comment - form
router.get("/new", middleware.isLoggedIn, function (req, res) {
    campSiteModel.findById(req.params.id, function (err, reqCamp) {
        if (err) {
            console.log(err)
        } else {
            res.render("comments/new", { camp: reqCamp });
        }
    })
});

//new comment
router.post("/", middleware.isLoggedIn, function (req, res) {
    var newComment = req.body.comment;
    campSiteModel.findById(req.params.id, function (err, reqCamp) {
        if (err) {
            console.log(err)
        } else {
            Comment.create(newComment, function (err, createdComment) {
                if (err) {
                    console.log(err);
                } else {
                    //add username and id to comment
                    createdComment.author.id = req.user._id;
                    createdComment.author.username = req.user.username;
                    createdComment.save();

                    reqCamp.comments.push(createdComment);
                    reqCamp.save();
                    console.log(createdComment);
                    req.flash("success", "Successfully added comment!");

                    res.redirect("/campsites/" + reqCamp._id);
                }
            })
        }
    })
});

//edit comment - get form for editing
router.get("/:comment_id/edit", middleware.checkCommentOwnership, function (req, res) {
    Comment.findById(req.params.comment_id, function (err, foundComment) {
        if (err) {
            console.log(err);
        } else {
            console.log(foundComment);
            console.log(req.params.id);
            res.render("comments/edit", { camp_id: req.params.id, comment: foundComment });

        }
    })

});

//update the comment
router.put("/:comment_id", middleware.checkCommentOwnership, function (req, res) {
    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, function (err, updatedComment) {
        if (err) {
            console.log(err);
        } else {
            req.flash("success", "Successfully edited comment!");

            res.redirect("/campsites/" + req.params.id);
        }
    })
});

//delete the comment
router.delete("/:comment_id", middleware.checkCommentOwnership, function(req, res) {
    Comment.findByIdAndDelete(req.params.comment_id, function(err) {
        if (err) {
            console.log(err);
        } else {
            req.flash("success", "Successfully deleted comment!");
            res.redirect("/campsites/" + req.params.id)
        }
    } )
})




module.exports = router;
