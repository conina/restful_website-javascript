var mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

//SCHEMA SETUP
var campsiteSchema = new mongoose.Schema(
    {
        name: String,
        price: String,
        image: String,
        description: String,
        author: {
            id: {
                type: mongoose.Schema.Types.ObjectId,
                ref: "User"
            },
            username: String
        },
        comments: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Comment"
        }]
        }
);

//CREATE MODEL
module.exports = mongoose.model("Camp", campsiteSchema);

